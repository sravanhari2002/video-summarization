import openai, os, tiktoken
import pandas as pd
from openai.embeddings_utils import get_embedding

openai.api_type = "azure"
openai.api_base = "https://jayasai01.openai.azure.com/"
openai.api_key = "d5cd9e479fb84ef698dead338e5b8a9b"
openai.api_version = "2023-05-15"
tokenizer = tiktoken.get_encoding("cl100k_base")
def split_into_many(text, max_tokens = 500):
    sentences = text.split('. ')
    n_tokens = [len(tokenizer.encode(" " + sentence)) for sentence in sentences]
    chunks = []
    tokens_so_far = 0
    chunk = []
    for sentence, token in zip(sentences, n_tokens):
        if tokens_so_far + token > max_tokens:
            chunks.append(". ".join(chunk) + ".")
            chunk = []
            tokens_so_far = 0
        if token > max_tokens:
            continue
        chunk.append(sentence)
        tokens_so_far += token + 1
    return chunks

def build_csv(video_file):
    shortened = []
    with open('Transcribed.txt','r') as r:
        text = r.read()
    df = pd.DataFrame(columns=['content','tokens','revised_content','revised_tokens','embeddings'])
    df['content'] = text
    df['tokens'] = df.content.apply(lambda x: len(tokenizer.encode(x)))
    for row in df.iterrows():
        if row[1]['content'] is None:
            continue
        if row[1]['tokens'] > 500:
            shortened += split_into_many(row[1]['content'])
        else:
            shortened.append(row[1]['content'])
    df['revised_content'] = pd.Series(shortened)
    df['revised_tokens'] = df.revised_content.apply(lambda x: len(tokenizer.encode(x)))
    df['embeddings'] = df.revised_content.apply(lambda x: get_embedding(x, engine='text-embedding-ada-002'))
    df.to_csv("Embeddings.csv")





