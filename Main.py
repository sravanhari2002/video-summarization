from Azure_Speech import implement
from Video2Text import Video_Text
from Embedding import *
import numpy as np
from openai.embeddings_utils import distances_from_embeddings

openai.api_type = "azure"
openai.api_base = "https://jayasai01.openai.azure.com/"
openai.api_key = "d5cd9e479fb84ef698dead338e5b8a9b"
openai.api_version = "2022-12-01"
video_file_name = 'GPT_4.mp4'

def create_context(
    question, df, max_len=1800, size="ada"
):
    """
    Create a context for a question by finding the most similar context from the dataframe
    """

    # Get the embeddings for the question
    q_embeddings = openai.Embedding.create(input=question, engine='text-embedding-ada-002')['data'][0]['embedding']

    # Get the distances from the embeddings
    df['distances'] = distances_from_embeddings(q_embeddings, df['embeddings'].values, distance_metric='cosine')


    returns = []
    cur_len = 0

    for i, row in df.sort_values('distances', ascending=True).iterrows():
        
        # Add the length of the text to the current length
        cur_len += row['revised_tokens'] + 4
        
        # If the context is too long, break
        if cur_len > max_len:
            break
        
        # Else add it to the text that is being returned
        returns.append(row["revised_content"])

    return "\n\n###\n\n".join(returns)

def answer_question(
    df,
    question="Am I allowed to publish model outputs to Twitter, without a human review?",
    max_len=1800,
    size="ada",
    debug=False,
    max_tokens=150,
    stop_sequence=None
):
    """
    Answer a question based on the most similar context from the dataframe texts
    """
    context = create_context(
        question,
        df,
        max_len=max_len,
        size=size,
    )

    try:
        # Create a completions using the question and context
        response = openai.Completion.create(
            prompt=f"Answer the question based on the context below, and if the question can't be answered based on the context, say \"I don't know\"\n\nContext: {context}\n\n---\n\nQuestion: {question}\nAnswer:",
            temperature=0,
            max_tokens=max_tokens,
            top_p=1,
            frequency_penalty=0,
            presence_penalty=0,
            stop=stop_sequence,
            engine='Interface',
        )
        return response.choices[0].text.strip()
    except Exception as e:
        print(e)
        return ""
if not os.path.exists('Trasncribed.txt'):
    Video_Text(video_file_name)
    build_csv(video_file_name)
    df=pd.read_csv('Embeddings.csv', index_col=0)
    df['embeddings'] = df['embeddings'].apply(eval).apply(np.array)
    Q = input("Ask Question : ")
    print(answer_question(df, question=Q))
elif not os.path.exists('Embeddings.csv'):
    build_csv(video_file_name)
    df=pd.read_csv('Embeddings.csv', index_col=0)
    df['embeddings'] = df['embeddings'].apply(eval).apply(np.array)
    Q = input("Ask Question : ")
    print(answer_question(df, question=Q))
else:
    df=pd.read_csv('Embeddings.csv', index_col=0)
    df['embeddings'] = df['embeddings'].apply(eval).apply(np.array)
    Q = input("Ask Question : ")
    print(answer_question(df, question=Q))