from pydub import AudioSegment
from moviepy.editor import *
from Azure_Speech import implement

def Video_Text(video_file):
    video = VideoFileClip(video_file)
    audio = video.audio
    wavfile = os.path.splitext(video_file)[0]+'.wav'
    audio.write_audiofile(wavfile)
    implement(wavfile)

